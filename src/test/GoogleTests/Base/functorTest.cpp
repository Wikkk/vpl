//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <VPL/Base/Functor.h>

namespace functor
{
//============== Test required objects ==================
//! Test local functions.
template <typename T>
T getMax(T a, T b)
{
	return (a > b) ? a : b;
}
template <typename T>
T getSquare(T a)
{
	return a * a;
}
template <typename T>
T getMultiply(T a, T b)
{
	return a * b;
}
//! Class for accessing methods for functor
template <typename T>
class TestClass
{
public:
    //! Default constructor
	TestClass() = default;

	//! Destructor
    virtual ~TestClass() = default;

	//! Class test methods
    static T square(T a)
    {
        return a * a;
    }
    static T multiply(T a, T b)
    {
        return a * b;
    }
	T getMax(T a, T b)
    {
        return (a > b) ? a : b;
    }

};

//!======================== Test code =================
//! Test fixture
template <typename T>
class FunctorTest : public ::testing::Test
{
public:
	using type = T;

	//! Default constructor
	FunctorTest() = default;

	//! Destructor
	virtual ~FunctorTest() = default;

	void SetUp() override {}
	void TearDown() override {}
};

//! Define types for testing.
typedef ::testing::Types<int, unsigned int,double> types;
TYPED_TEST_CASE(FunctorTest, types);


//! Testing functor for local functions.
TYPED_TEST(FunctorTest,LocalMethod)
{
   	using type = typename TestFixture::type;

    vpl::base::CFunctor<type, type, type> max(getMax<type>);
    ASSERT_FLOAT_EQ(10,max(5, 10.0));

	vpl::base::CFunctor<type, type> square(getSquare<type>);
	ASSERT_FLOAT_EQ(100, square(10.0));

	vpl::base::CFunctor<type, type, type> multiply(getMultiply<type>);
	ASSERT_FLOAT_EQ(12, multiply(2.0, 6.0));
}
//! Testing functor for not static class functions.
TYPED_TEST(FunctorTest,NonStaticClassMethod)
{
  	using type = typename TestFixture::type;

	TestClass<type> test;
    vpl::base::CFunctor<type, type, type> max = vpl::base::CFunctor<type, type, type>(&test, &TestClass<type>::getMax);
    ASSERT_FLOAT_EQ(15, max(15.0f, 10));
}

//! Testing functor for static class functions.
TYPED_TEST(FunctorTest,StaticClassMethod)
{
   	using type = typename TestFixture::type;

    vpl::base::CFunctor<type, type> square(TestClass<type>::square);
    ASSERT_EQ(100,square(10));
	vpl::base::CFunctor<type, type, type> multiply(TestClass<type>::multiply);
	ASSERT_EQ(50, multiply(10, 5));
}

} // functor