//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/


#include "gtest/gtest.h"
#include "VPL/Image/Image.h"
#include <VPL/Test/Utillities/imageMetrics.h>
#include "VPL/Module/DensityCompressor.h"
#include <VPL/Image/ImageFunctions.h>
#include <VPL/ImageIO/PNG.h>
#include <VPL/Test/Utillities/arguments.h>
#include "VPL/Image/Filters/Gaussian.h"
#include "VPL/Test/Utillities/referenceData.h"
#include <VPL/Base/Types.h>
#include <VPL/Test/Utillities/testingData.h>

namespace filters
{
//! Testing filters on CImage16 
class Image16Filter : public testing::Test
{
public:
	
	std::string dirPath;

	vpl::img::CImage16Ptr inImage;
	vpl::test::ReferenceData config;
	vpl::img::CImage16Ptr image;
	bool isConfigLoaded{};

	void SetUp() override
	{
        dirPath = vpl::test::TestingData::getDirectoryPath() + "/data";

	}
	void LoadImage(vpl::img::CImage16& image, const std::string& fileName) const
	{
		const vpl::mod::CFileChannelPtr inChannel = vpl::mod::CFileChannelPtr(
			new vpl::mod::CFileChannel(vpl::mod::CH_IN, dirPath + '/' + fileName));

		if (!loadPNG(image, *inChannel))
		{
			FAIL() << "Image: " << dirPath << '/' << fileName << " is not loaded";
		}
	}

	//! Fix margin to required size for filter kernel
	static void fixMargin(vpl::img::CImage16Ptr& image, const int marginSize)
	{
		if (image->getMargin() < marginSize)
		{
			const vpl::tSize sx(image->getXSize());
			const vpl::tSize sy(image->getYSize());

			// Create data
			vpl::img::CImage16Ptr s(new vpl::img::CImage16(sx, sy, marginSize));

			// Copy source
			for (vpl::tSize y = 0; y < sy; ++y)
				for (vpl::tSize x = 0; x < sx; ++x)
					s->set(x, y, image->at(x, y));
			// Create margin
			s->mirrorMargin();
			// Store slice
			image = s;
		}
	}
};

//! Tesing if comparation method is OK.
TEST_F(Image16Filter, CheckTestingCompareMethod)
{

	LoadImage(*inImage, "images/lena.png");
	LoadImage(*image, "images/lena.png");
	double metrics;
	ASSERT_TRUE(vpl::test::metrics::ad(*inImage, *image, metrics));
	ASSERT_FLOAT_EQ(0, metrics) << "Image metrics ad failed";

	ASSERT_TRUE(vpl::test::metrics::psnr(*inImage, *image, metrics)) << "Failed comparing images";
	ASSERT_EQ(INFINITY, metrics) << "Image metrics psnr failed";

}

//! Testing gauss filter
TEST_F(Image16Filter, Gauss)
{
	//! Check if is loaded config file
	ASSERT_TRUE(config.load(dirPath, "images/gauss")) << "No config loaded";
	//! Get default input image
	std::string commonInput;
	if (config.getGlobalInput(commonInput))
	{
		LoadImage(*inImage, "images/" + commonInput);
	}

	//! Iterate over all test from config file
	for (int testId = 0; testId < config.getTestCount(); testId++)
	{
		vpl::img::CImage16Ptr testImage(new vpl::img::CImage16(*inImage));
		std::string input;
		//! Is default image overrided?
		if (config.getInput(input, testId))
		{
			const vpl::img::CImage16Ptr inputImage;
			LoadImage(*inputImage, "images/" + input);
			testImage = inputImage;
		}
		//! Get required arguments from config file
		const std::string name = config.getString("name", testId);
		const std::string refImage = config.getString("reference", testId);
		const float sigma = config.getNumber<float>("sigma", testId);
		int psnrRequired = config.getNumber<int>("psnr", testId);

		//! Run filter
		vpl::img::CGaussFilter<vpl::img::CImage16> filter(sigma);
		LoadImage(*image, "images/" + refImage);

		fixMargin(testImage, filter.getSize() >> 1);

		const vpl::img::CImage16Ptr spFiltered(new vpl::img::CImage16(testImage->getXSize(), testImage->getYSize()));
		filter(*testImage, *spFiltered);

		//! Check output with reference
		double psnr;
		ASSERT_TRUE(vpl::test::metrics::psnr(*spFiltered, *image, psnr)) << "Failed comparing images in test:" << name;
		EXPECT_GE(psnr, psnrRequired) << "Image metrics psnr failed in test:" << name;
	}
}
}
