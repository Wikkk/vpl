//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <VPL/Base/Setup.h>
#include <VPL/Module/Signal.h>


namespace signals
{

std::string reqString;

//! Methods for register local methods
double getMax(double a, double b)
{
	return (a > b) ? a : b;
}

void print1(const int a)
{
    reqString += "void print1(" + std::to_string(a) + ")#";
}

void printHello()
{
    reqString += "Hello ";
}

//! Test class for register class methods
class CTest
{
public:
    //! Default constructor
    CTest() {}

    //! Destructor
    virtual ~CTest() {}

    //! Method
    virtual void print1(const int a)
    {
        reqString += "void CTest::print1(" + std::to_string(a) + ")#";
    }

    //! Method
    void printWorld() const
    {
        reqString += "world!";
    }
};




//! Testing connection to local methods and results of invoking.
TEST(SignalTest, Local)
{

	//! Create a new signal sig0
	vpl::mod::CSignal<double, double, double> sig0;

	//! Register a new handler (getMax() function)
	vpl::mod::tSignalConnection con0 = sig0.connect(getMax);

	//! Invoke the signal
	EXPECT_EQ(5, sig0.invoke2(5, 2));

	//! Deregister the getMax() function and invoke
	sig0.disconnect(con0);
	EXPECT_EQ(0, sig0.invoke2(5, 2));

}

//! Testing connection to class methods and results of invoking.
TEST(SignalTest,Class)
{
    CTest test;
    //! Create a new signal sig1
    vpl::mod::CSignal<void> sig1;

    //! Register a new handlers (printHello() and CTest::printWorld() functions)
    vpl::mod::tSignalConnection con10 = sig1.connect(printHello);
    vpl::mod::tSignalConnection con11 = sig1.connect(&test, &CTest::printWorld);

    //! Invoke the signal
    sig1.invoke();

    ASSERT_STREQ("Hello world!", reqString.c_str());
    reqString.clear();

    //! Block the CTest::printWorld() function and invoke
    sig1.block(con11);
    sig1.invoke();

    ASSERT_STREQ("Hello ", reqString.c_str());
    reqString.clear();
  

    vpl::mod::CSignal<void, int> sig2;

    //! Register a new handlers (print1() and CTest::print1() functions)
    vpl::mod::tSignalConnection con20 = sig2.connect(print1);
    vpl::mod::tSignalConnection con21 = sig2.connect(&test, &CTest::print1);

    //! Invoke the signal
    sig2.invoke(20);
    
    ASSERT_STREQ("void print1(20)#void CTest::print1(20)#", reqString.c_str());
    reqString.clear();

    //! Deregister both handlers and invoke the signal
    sig2.disconnect(con20);
    sig2.disconnect(con21);
}
}