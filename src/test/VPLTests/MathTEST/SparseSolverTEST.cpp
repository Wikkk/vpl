//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk)    \n
 * Copyright (c) 2003-2007 by PGMed@FIT         \n
 *
 * Author:  Miroslav Svub, svub@fit.vutbr.cz    \n
 * Date:    2006/06/27                          \n
 *
 * Description:
 * - Testing of the vpl::math::CSparseMatrix template.
 */

#include <VPL/Math/SparseSystem.h>

// STL
#include <ctime>
#include <iostream>


//==============================================================================
/*
 * Global definitions.
 */

//! Vector of doubles.
typedef vpl::math::CVector<double> tVector;

//! Sparse matrix of doubles. 
//typedef vpl::math::CSparseMatrix<double> tMatrix;
typedef vpl::math::CSparseMatrixAlt<double> tMatrix;


//==============================================================================
/*!
 * Prints a given vector
 */
void printVector(tVector& v)
{
    std::cout.setf(std::ios_base::fixed);
    std::cout << "  ";
    for( vpl::tSize i = 0; i < v.getSize(); i++ )
    {
        std::cout << v(i) << " ";
    }
    std::cout << std::endl;
}


//==============================================================================
/*!
 * Prints a given matrix
 */
void printMatrix(tMatrix& m)
{
    std::cout.setf(std::ios_base::fixed);
    for( vpl::tSize i = 0; i < m.getNumOfRows(); i++ )
    {
        std::cout << "  ";
        for( vpl::tSize j = 0; j < m.getNumOfCols(); j++ )
        {
            std::cout << m(i,j) << " ";
        }
        std::cout << std::endl;
    }
}


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    tMatrix M(5, 5, 0.0);
    tVector v1(5);
    tVector v2(5);
    tVector v3(5);

    M.set(0, 0, 2.0);
    M.set(1, 0, 3.0);
    M.set(0, 1, 3.0);
    M.set(2, 1, -1.0);
    M.set(4, 1, 4.0);
    M.set(1, 2, 4.0);
    M.set(2, 2, -3.0);
    M.set(3, 2, 1.0);
    M.set(4, 2, 2.0);
    M.set(2, 3, 2.0);
    M.set(1, 4, 6.0);
    M.set(4, 4, 1.0);

    std::cerr << "Sparse matrix A (dominant value = 0):" << std::endl;
    printMatrix(M);
    keypress();

    std::cerr << "Iterate over all non-dominant values:" << std::endl;
    for( tMatrix::tIterator It(M); It; ++It )
    {
        std::cerr << " " << *It;
    }
    std::cerr << std::endl;
    keypress();

    v1(0) = 8.0;
    v1(1) = 45.0;
    v1(2) = -3.0;
    v1(3) = 3.0;
    v1(4) = 19.0;

    std::cerr << "Right hand side:  b = [";
    for ( int i = 0; i < 5; i++ ) std::cerr << " " << v1(i);
    std::cerr << " ]" << std::endl;
    keypress();

    vpl::math::solve<double>(M, v1, v2);

    std::cerr << "Solution vector (Ax = b):  x = [";
    for ( int i = 0; i < 5; i++ ) std::cerr << " " << v2(i);
    std::cerr << " ]" << std::endl;
    keypress();

    M.rmult(v2, v3);

    std::cerr << "Test:             Ax = [";
    for ( int i = 0; i < 5; i++ ) std::cerr << " " << v3(i);
    std::cerr << " ]" << std::endl;
    keypress();

    return 0;
}
