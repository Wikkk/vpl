//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/11/04                       
 *
 * Description:
 * - Testing of the vpl::CFileBrowser class.
 */

#include <VPL/System/FileBrowser.h>

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>

// STL
#include <iostream>
#include <string>


//==============================================================================
/*
 * Global constants and variables.
 */

//! Maximal length of user input
const int MAX_USER_INPUT    = 64;

//! The number of items listed in a one page
const int PAGE_LENGTH       = 10;

//! Smart pointer to file browser
vpl::sys::CFileBrowserPtr spFileBrowser;


//==============================================================================
int main(int argc, char* argv[])
{
    char pcTemp[MAX_USER_INPUT];

    // Get the current directory
    std::cout << "Current directory: " << spFileBrowser->getDirectory() << std::endl;

    // Read the directory
    std::cout << "Enter the directory: ";
    fgets(pcTemp, MAX_USER_INPUT, stdin);
    pcTemp[strlen(pcTemp) - 1] = '\0';
    std::string sDirectory = pcTemp;

    // Change the current directory
    if (!spFileBrowser->setDirectory(sDirectory))
    {
        std::cout << "Error: cannot change the directory" << std::endl;
        return 0;
    }

    // Read the filemask
    std::cout << "Enter the filemask: ";
    fgets(pcTemp, MAX_USER_INPUT, stdin);
    pcTemp[strlen(pcTemp) - 1] = '\0';
    std::string sFilemask = pcTemp;

    // List the directory
    std::cout << "Directory listing:" << std::endl;
    vpl::sys::CFileBrowser::SFileAttr File;
    int iCount = 0;
    bool bResult = spFileBrowser->findFirst(sFilemask, File);
    while( bResult )
    {
        std::cout << ((File.m_bDirectory) ? "  d " : "    ") << File.m_sName << std::endl;

        if( (++iCount % PAGE_LENGTH) == 0 )
        {
            std::cout << "Press Enter to continue..." << std::endl;
            std::cin.get();
        }

        bResult = spFileBrowser->findNext(File);
    }

    return 0;
}

