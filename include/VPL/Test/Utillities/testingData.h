//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#pragma once
#include <string>

namespace vpl {
namespace test {

//! Contains settings for data location for testing.
class TestingData
{
public:

    //! Initialize argument
    static void init(int argc, char *argv[]);

    //! Return path to directory with test data. 
    //! Path may have 3 values form function:
    //! 1. Value from -dir argument if is used.
    //! 2. If dir is mounted to T: disk this path is used. 
    //! 3. Other: current dir "./"
    static std::string getDirectoryPath();
};

} // namespace test
} // namespace vpl